package com.test;

import org.junit.Assert;
import org.junit.Test;

import com.factory.RobotFactory;
import com.model.Robot;
import com.service.RobotService;

/**
 * JUnit test class
 * 
 * @author sumitarora
 *
 */
public class TestRopot {
	
	@Test
	public void doTest(){
		RobotFactory rf = new RobotFactory();
		Robot robo1 = rf.getInstance("TestRobot");
		RobotService rs = rf.getImplementation();
		
		System.out.println("Output of Test case 1-A");
		//Test case 1-A
		rs.addCharging(100);
		double walkDone = 3.5;
		boolean status = rs.getRemainingBattery(walkDone, robo1);
		Assert.assertTrue(status);
		
		System.out.println("\nOutput of Test case 1-B");
		//Test case 1-B
		rs.addCharging(100);
		//walkDone = 6;				//If uncomment this line then test will fail
		status = rs.getRemainingBattery(walkDone, robo1);
		Assert.assertTrue(status);
		
		
		System.out.println("\nOutput of Test case 2-A");
		//Test case 2-A
		rs.addCharging(100);
		walkDone = 2;
		int carryingWeight = 3;
		status = rs.getRemainingBattery(walkDone, carryingWeight, robo1);
		Assert.assertTrue(status);
		
		
		System.out.println("\nOutput of Test case 2-b");
		//Test case 2-B	: //I have recharged the Robot so it will work
		rs.addCharging(100);		//If this line is commented then test case will fail coz 54% battery is left but required batter is 96%
		walkDone = 4.5;
		carryingWeight = 3;
		status = rs.getRemainingBattery(walkDone, carryingWeight, robo1);
		Assert.assertTrue(status);
		
		
		System.out.println("\nOutput of Test case 3-A");
		//Test case 3-A
		rs.addCharging(100);
		walkDone = 0;
		carryingWeight = 10;		//Valid weight value
		status = rs.getRemainingBattery(walkDone, carryingWeight, robo1);
		Assert.assertTrue(status);
		
		System.out.println("\nOutput of Test case 3-B");
		//Test case 3-B
		rs.addCharging(100);
		//carryingWeight = 12;		//If uncomment this line then test case will fail because its overweight
		status = rs.getRemainingBattery(walkDone, carryingWeight, robo1);
		Assert.assertTrue(status);
		
	}

}
