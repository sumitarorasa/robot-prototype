package com.utility;

/**
 * Color enum class
 * 
 * @author sumitarora
 *
 */
public enum Color {
	RED("red"), GREEN("green");
	
	private String color;
	
	public String getColor() {
		return color;
	}

	void setColor(String color) {
		this.color = color;
	}

	
	private Color(String color){
		this.color = color;
	}

}
