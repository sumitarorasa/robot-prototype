package com.utility;

/**
 * Get application constant
 * 
 * @author sumitarora
 *
 */
public class AppConstant {
	
	public static final int FULL_BATTERY_PERCENTAGE = 100;
	public static final int ZERO_BATTERY_PERCENTAGE = 0;
	public static final String INSUFFICIENT_BATTERY = "Insufficient battery";
	public static final String FULL_BATTERY = "Already full battery, no need to charge it.";
	public static final String CHARGING_DONE_SUCCESS = "Charging done.";
	public static final String CURRENT_BATTERY_STATUS_IS = "Current battery status is: ";
	public static final String ONE_CHARACTER_SPACE = " ";
	public static final String PERCENTAGE_SYMBOL = "%";
	public static final int PER_KG_BATTERY_CONSUMPTION = 2;
	public static final String WALKING_DONE_SUCCESS = " walking done.";
	public static final String BATTERY_CONSUMED = "Battery consumed: ";
	public static final String COMMA_WITH_ONE_CHARACTER_SPACE = ", ";
	public static final String WALKING_DONE_WITH_WEIGHT_SUCCESS = " walking with weight done.";
	public static final String PLEASE_RECHARGE = "Please recharge it.";
	public static final String FULL_STOP = ".";
	public static final int MAX_CARRYING_WEIGHT_CAPACITY = 10;
	public static final String INVALID_INPUT = "Invalid inputs.";
	public static final int LOW_BATTERY_PERCENTAGE = 15;
	public static final String LOW_BATTERY_MSG = "Battery is low.";
	public static final String HEAD_LIGHT_BLINKING_WITH = " Head light is blinking with ";
	public static final String COLOR = " color.";
	public static final String OVERWEIGHT_MSG = "Its overweight. Please decrease my carrying weight.";
	public static final String SCANNING_SUCCESS_MSG = "Scanning success";
	public static final String SCANNING_FAILUR_MSG = "Scanning failure";
	public static final String SUCCESSFULLY_CARRIED_WEIGHT = " successfully carried the weight.";
	
	private AppConstant(){
		
	}

}
