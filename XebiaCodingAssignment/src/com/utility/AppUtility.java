package com.utility;

import java.util.List;

/**
 * Utility class to provide helper methods
 * 
 * @author sumitarora
 *
 */
public class AppUtility {
	
	private AppUtility(){
		//
	}
	
	/**
	 * Get formatted message
	 * 
	 * @param list
	 * @return
	 */
	
	public static String getMessage(List<String> list) {
		StringBuilder sb = new StringBuilder();
		for(String str : list){
			sb.append(str);
		}
		return sb.toString();
	}
	
	/**
	 * Get battery value
	 * 
	 * @param walk
	 * @return
	 */
	public static Integer getBatteryValPerHalfKM(double walk){
		int countWalkPerHalfKM = (int) (walk / 0.5);
		return countWalkPerHalfKM * 10;
	}
	
	/**
	 * Show output
	 * 
	 * @param message
	 */
	public static void showOutput(String message) {
		System.out.println(message);
	}
	
	/**
	 * Check is the barcode clear or not
	 * 
	 * @param apiBarcode
	 * @return
	 */
	public static boolean isClear(String apiBarcode) {
		return true;
	}
}
