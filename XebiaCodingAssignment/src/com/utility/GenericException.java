package com.utility;

/**
 * 
 * This class server the purpose of exception handling
 * 
 * @author sumitarora
 *
 */

public class GenericException extends RuntimeException {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	GenericException(){
		super();
	}
	
	
	public GenericException(String msg){
		super(msg);
	}
}
