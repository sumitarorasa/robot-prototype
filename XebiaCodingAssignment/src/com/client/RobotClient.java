package com.client;

import com.factory.RobotFactory;
import com.model.Robot;
import com.service.RobotService;

/**
 * This class will server as a client for Robot prototype
 * 
 * @author sumitarora
 *
 */
public class RobotClient {

	public static void main(String[] args) {
		
		RobotFactory rf = new RobotFactory();		//Create factory object
		Robot robo1 = rf.getInstance("Robot1");		//Create object
		RobotService rs = rf.getImplementation();	//Get implementation
		rs.showBatteryStatus();						//Show battery status
		rs.addCharging(100);						//Add charging
		double walkDone = 4.5;						//initial value
		rs.getRemainingBattery(walkDone, robo1);	//Get remaining battery value without carrying weight	
		rs.addCharging(100);						//Add charging
		rs.showBatteryStatus();						//Show battery status
		int carryingWeight = 6;						//initial value
		walkDone = 1;								//re assign value for different scenario	
		rs.getRemainingBattery(walkDone, carryingWeight, robo1);		//Get remaining battery with carrying weight
		rs.getRemainingBattery(walkDone, robo1);						//Get remaining battery
		carryingWeight = 12;											//re assign value for different scenario	
		rs.getRemainingBattery(walkDone, carryingWeight, robo1);		//Get remaining battery with carrying weight
	}

}
