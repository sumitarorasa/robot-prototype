package com.service.impl;

import java.util.Arrays;
import java.util.List;

import com.model.Robot;
import com.service.RobotService;
import com.utility.AppConstant;
import com.utility.AppUtility;
import com.utility.Color;
import com.utility.GenericException;

public class RobotServiceImpl implements RobotService {

	private int currentBattery = 0;
	
	
	@Override
	public void addCharging(int chargeVal) {
		try {
			throwBatteryFullException();
		} catch(GenericException e){
			AppUtility.showOutput(e.getMessage());
			return;
		}
		currentBattery += chargeVal;
		if(currentBattery > AppConstant.FULL_BATTERY_PERCENTAGE){
			currentBattery = AppConstant.FULL_BATTERY_PERCENTAGE;
		}
		List<String> list = Arrays.asList(AppConstant.CHARGING_DONE_SUCCESS, AppConstant.ONE_CHARACTER_SPACE, AppConstant.CURRENT_BATTERY_STATUS_IS, Integer.toString(currentBattery), AppConstant.PERCENTAGE_SYMBOL);
		AppUtility.showOutput(AppUtility.getMessage(list));
	}
	

	@Override
	public void showBatteryStatus() {
		List<String> list = Arrays.asList(AppConstant.CURRENT_BATTERY_STATUS_IS, Integer.toString(currentBattery), AppConstant.PERCENTAGE_SYMBOL);
		AppUtility.showOutput(AppUtility.getMessage(list));
	}
	
	@Override
	public boolean getRemainingBattery(double walkDone, Robot robot1) {
		int carryingWeight = 0;
		try {
			throwInvalidValueException(walkDone, carryingWeight);
		} catch(GenericException e){
			AppUtility.showOutput(e.getMessage());
			return false;
		}
		int batteryRequired = batteryRequired(walkDone, carryingWeight);
		try {
			throwInsufficientBatteryException(batteryRequired);
		} catch(GenericException e){
			AppUtility.showOutput(e.getMessage());
			return false;
		}
		currentBattery -= batteryRequired;
		List<String> list = Arrays.asList(robot1.getName(), AppConstant.WALKING_DONE_SUCCESS, AppConstant.ONE_CHARACTER_SPACE, AppConstant.BATTERY_CONSUMED, Integer.toString(batteryRequired), AppConstant.PERCENTAGE_SYMBOL, AppConstant.COMMA_WITH_ONE_CHARACTER_SPACE, AppConstant.CURRENT_BATTERY_STATUS_IS, Integer.toString(currentBattery), AppConstant.PERCENTAGE_SYMBOL);
		if(currentBattery == AppConstant.ZERO_BATTERY_PERCENTAGE){
			list = Arrays.asList(robot1.getName(), AppConstant.WALKING_DONE_SUCCESS, AppConstant.ONE_CHARACTER_SPACE, AppConstant.BATTERY_CONSUMED, Integer.toString(batteryRequired), AppConstant.PERCENTAGE_SYMBOL, AppConstant.COMMA_WITH_ONE_CHARACTER_SPACE, AppConstant.CURRENT_BATTERY_STATUS_IS, Integer.toString(currentBattery), AppConstant.PERCENTAGE_SYMBOL, AppConstant.FULL_STOP, AppConstant.ONE_CHARACTER_SPACE, AppConstant.PLEASE_RECHARGE);
		} else if(currentBattery < AppConstant.LOW_BATTERY_PERCENTAGE){
			robot1.setColor(Color.RED);
			list = Arrays.asList(robot1.getName(), AppConstant.WALKING_DONE_SUCCESS, AppConstant.ONE_CHARACTER_SPACE, AppConstant.BATTERY_CONSUMED, Integer.toString(batteryRequired), AppConstant.PERCENTAGE_SYMBOL, AppConstant.COMMA_WITH_ONE_CHARACTER_SPACE, AppConstant.CURRENT_BATTERY_STATUS_IS, Integer.toString(currentBattery), AppConstant.PERCENTAGE_SYMBOL, AppConstant.FULL_STOP, AppConstant.ONE_CHARACTER_SPACE, AppConstant.LOW_BATTERY_MSG, AppConstant.HEAD_LIGHT_BLINKING_WITH, robot1.getColor().toString(), AppConstant.COLOR, AppConstant.ONE_CHARACTER_SPACE, AppConstant.PLEASE_RECHARGE);
		}
			
		AppUtility.showOutput(AppUtility.getMessage(list));
		return true;
	}
	
	
	@Override
	public boolean getRemainingBattery(double walkDone, int carryingWeight, Robot robot1) {
		try {
			throwOverweightException(carryingWeight);
		} catch(GenericException e){
			AppUtility.showOutput(e.getMessage());
				return false;
			}
		int batteryRequired = batteryRequired(walkDone, carryingWeight);
		try {
			throwInsufficientBatteryException(batteryRequired);
		} catch(GenericException e){
			AppUtility.showOutput(e.getMessage());
			return false;
		}
		currentBattery -= batteryRequired;
		
		StringBuilder sb = new StringBuilder(AppConstant.WALKING_DONE_WITH_WEIGHT_SUCCESS);
		if(walkDone == AppConstant.ZERO_BATTERY_PERCENTAGE){
			sb = new StringBuilder(AppConstant.SUCCESSFULLY_CARRIED_WEIGHT);
		}
		
		List<String> list = Arrays.asList(robot1.getName(), sb.toString(), AppConstant.ONE_CHARACTER_SPACE, AppConstant.BATTERY_CONSUMED, Integer.toString(batteryRequired), AppConstant.PERCENTAGE_SYMBOL, AppConstant.COMMA_WITH_ONE_CHARACTER_SPACE, AppConstant.CURRENT_BATTERY_STATUS_IS, Integer.toString(currentBattery), AppConstant.PERCENTAGE_SYMBOL);
		
		if(currentBattery == AppConstant.ZERO_BATTERY_PERCENTAGE){
			list = Arrays.asList(robot1.getName(), AppConstant.WALKING_DONE_SUCCESS, AppConstant.ONE_CHARACTER_SPACE, AppConstant.BATTERY_CONSUMED, Integer.toString(batteryRequired), AppConstant.PERCENTAGE_SYMBOL, AppConstant.COMMA_WITH_ONE_CHARACTER_SPACE, AppConstant.CURRENT_BATTERY_STATUS_IS, Integer.toString(currentBattery), AppConstant.PERCENTAGE_SYMBOL, AppConstant.FULL_STOP, AppConstant.ONE_CHARACTER_SPACE, AppConstant.PLEASE_RECHARGE);
		} else if(currentBattery < AppConstant.LOW_BATTERY_PERCENTAGE){
			robot1.setColor(Color.RED);
			list = Arrays.asList(robot1.getName(), AppConstant.WALKING_DONE_SUCCESS, AppConstant.ONE_CHARACTER_SPACE, AppConstant.BATTERY_CONSUMED, Integer.toString(batteryRequired), AppConstant.PERCENTAGE_SYMBOL, AppConstant.COMMA_WITH_ONE_CHARACTER_SPACE, AppConstant.CURRENT_BATTERY_STATUS_IS, Integer.toString(currentBattery), AppConstant.PERCENTAGE_SYMBOL, AppConstant.FULL_STOP, AppConstant.ONE_CHARACTER_SPACE, AppConstant.LOW_BATTERY_MSG, AppConstant.HEAD_LIGHT_BLINKING_WITH, robot1.getColor().toString(), AppConstant.COLOR, AppConstant.ONE_CHARACTER_SPACE, AppConstant.PLEASE_RECHARGE);
		}
		AppUtility.showOutput(AppUtility.getMessage(list));
		return true;
	}
	
	@Override
	public void scannBarCode(String apiBarcode, Robot robot) {
		String scanMsg = (AppUtility.isClear(apiBarcode)) ?  "<price>" : AppConstant.SCANNING_FAILUR_MSG;
		List<String> list = Arrays.asList("Price for barcode: ", apiBarcode, " is: ", scanMsg);
		AppUtility.showOutput(AppUtility.getMessage(list));	
	}


	

	/**
	 * Get battery required value after some calculation
	 * 
	 * @param walk
	 * @param carryingWeight
	 * @return
	 */
	private int batteryRequired(double walk, int carryingWeight) {
		int carryingWeightRequiredBattery = carryingWeight * AppConstant.PER_KG_BATTERY_CONSUMPTION;
		int walkRequiredBattery = AppUtility.getBatteryValPerHalfKM(walk);
		return carryingWeightRequiredBattery + walkRequiredBattery; 
	}
	
	
	/**
	 * Throw exception if overweight
	 * 
	 * @param carryingWeight
	 */
	private void throwOverweightException(int carryingWeight) {
		if(carryingWeight > AppConstant.MAX_CARRYING_WEIGHT_CAPACITY){
			List<String> list = Arrays.asList(AppConstant.OVERWEIGHT_MSG);
			throw new GenericException(AppUtility.getMessage(list));
		}
	}
	
	/**
	 * Throw exception if invalid values are passed
	 * 
	 * @param walkDone
	 * @param carryingWeight
	 */
	private void throwInvalidValueException(double walkDone, int carryingWeight) {
		if(walkDone <= 0 && carryingWeight <= 0){
			List<String> list = Arrays.asList(AppConstant.INVALID_INPUT);
			throw new GenericException(AppUtility.getMessage(list));
		}
	}
	
	/**
	 * Throw if battery is insufficient
	 * 
	 * @param batteryRequired
	 */
	private void throwInsufficientBatteryException(int batteryRequired) {
		if(batteryRequired > currentBattery ){
			List<String> list = Arrays.asList(AppConstant.INSUFFICIENT_BATTERY);
			throw new GenericException(AppUtility.getMessage(list));
		}
	}
	
	/**
	 * Throw battery full exception
	 * 
	 */
	private void throwBatteryFullException() {
		if(currentBattery == AppConstant.FULL_BATTERY_PERCENTAGE){
			List<String> list = Arrays.asList(AppConstant.FULL_BATTERY);
			throw new GenericException(AppUtility.getMessage(list));
		}
	}
}
