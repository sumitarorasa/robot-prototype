package com.service;

import com.model.Robot;

public interface RobotService {
	
	/**
	 * Add charging value in Robot 
	 * 
	 * @param chargeVal
	 */
	public void addCharging(int chargeVal);
	
	/**
	 * Get battery status
	 * 
	 */
	public void showBatteryStatus();

	/**
	 * Get remaining batter detail after doing some task without carrying weight
	 * 
	 * @param walkDone
	 * @param robot1
	 * @return
	 */
	public boolean getRemainingBattery(double walkDone, Robot robot1);
	
	/**
	 * Get remaining batter detail after doing some task with carrying weight
	 * 
	 * @param walkDone
	 * @param carryingWeight
	 * @param robot1
	 * @return
	 */
	public boolean getRemainingBattery(double walkDone, int carryingWeight, Robot robot1);
	
	/**
	 * Scann barcode
	 * 
	 * @param apiBarcode
	 * @param robot
	 */
	public void scannBarCode(String apiBarcode, Robot robot);

}
