package com.model;

import com.utility.Color;

/**
 * This class will work as base entity for Robot
 * 
 * @author sumitarora
 *
 */
public class Robot {
	
	private String name;
	private Color headLightColor;
	private static final int MAX_WALK_PER_CHARGING = 5;			
			
	public Robot(String name) {
		super();
		this.name = name;
		this.headLightColor = Color.GREEN;
	}

	public String getName() {
		return name;
	}

	public Color getColor() {
		return headLightColor;
	}

	public static int getMaxWalkPerCharging() {
		return MAX_WALK_PER_CHARGING;
	}


	public void setName(String name) {
		this.name = name;
	}

	public void setColor(Color color) {
		this.headLightColor = color;
	}

}
