package com.factory;

import com.model.Robot;
import com.service.RobotService;
import com.service.impl.RobotServiceImpl;

public class RobotFactory {
	
	/**
	 * Get Robot instance
	 * 
	 * @param name
	 * @return
	 */
	public Robot getInstance(String name){
		return new Robot(name);
	}
	
	
	/**
	 * Get RobotService implementation
	 * 
	 * @return
	 */
	public RobotService getImplementation(){
		return new RobotServiceImpl();
	}
	
	
}
